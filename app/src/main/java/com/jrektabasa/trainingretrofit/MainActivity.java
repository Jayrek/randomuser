package com.jrektabasa.trainingretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jrektabasa.trainingretrofit.adapter.RandomUserAdapter;
import com.jrektabasa.trainingretrofit.api.ApiClient;
import com.jrektabasa.trainingretrofit.api.ApiInterface;
import com.jrektabasa.trainingretrofit.model.RandomUserResponse;
import com.jrektabasa.trainingretrofit.model.ResultsResponse;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private ResultsResponse resultsResponse;
    private ArrayList<RandomUserResponse> randomUserResponseArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

//        ivRandomUserPhoto = (CircleImageView) findViewById(R.id.ivRandomUserPhoto);
//        tvRandomName = (TextView) findViewById(R.id.tvRandomName);
//        btnGenerate = (Button) findViewById(R.id.btnGenerate);
//        btnGenerate.setOnClickListener(this);
        getRandomUser();
    }

    public void displayUsers(ArrayList<RandomUserResponse> randomUserResponseArrayList) {
        RandomUserAdapter randomUserAdapter = new RandomUserAdapter(randomUserResponseArrayList, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView.setAdapter(randomUserAdapter);
        randomUserAdapter.notifyDataSetChanged();
    }

    public void getRandomUser() {
        ApiClient apiClient = new ApiClient();
        ApiInterface apiInterface = apiClient.getRetro();
        Call<ResultsResponse> call = apiInterface.randomUserResponseCall(10);
        call.enqueue(new Callback<ResultsResponse>() {
            @Override
            public void onResponse(Call<ResultsResponse> call, Response<ResultsResponse> response) {
                resultsResponse = response.body();
                if (response.isSuccessful()) {
                    randomUserResponseArrayList = resultsResponse.getRandomUserResponseArrayList();
                    displayUsers(randomUserResponseArrayList);
                }
            }

            @Override
            public void onFailure(Call<ResultsResponse> call, Throwable t) {
                Log.wtf("onFailure", "Failed");
            }
        });


    }

}

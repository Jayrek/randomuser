package com.jrektabasa.trainingretrofit.api;

import com.jrektabasa.trainingretrofit.model.ResultsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("api/1.1/")
    Call<ResultsResponse> randomUserResponseCall(@Query("results") int intResults);
}

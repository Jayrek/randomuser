package com.jrektabasa.trainingretrofit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResultsResponse {
    @SerializedName("results")
    private ArrayList<RandomUserResponse> randomUserResponseArrayList;

    public ResultsResponse(ArrayList<RandomUserResponse> randomUserResponseArrayList) {
        this.randomUserResponseArrayList = randomUserResponseArrayList;
    }

    public ArrayList<RandomUserResponse> getRandomUserResponseArrayList() {
        return randomUserResponseArrayList;
    }

    public void setRandomUserResponseArrayList(ArrayList<RandomUserResponse> randomUserResponseArrayList) {
        this.randomUserResponseArrayList = randomUserResponseArrayList;
    }
}

package com.jrektabasa.trainingretrofit.model;

import com.google.gson.annotations.SerializedName;

public class PictureResponse {
    @SerializedName("large")
    private String strLarge;
    @SerializedName("medium")
    private String strMedium;
    @SerializedName("thumbnail")
    private String strThumbnail;

    public PictureResponse(String strLarge, String strMedium, String strThumbnail) {
        this.strLarge = strLarge;
        this.strMedium = strMedium;
        this.strThumbnail = strThumbnail;
    }

    public String getStrLarge() {
        return strLarge;
    }

    public void setStrLarge(String strLarge) {
        this.strLarge = strLarge;
    }

    public String getStrMedium() {
        return strMedium;
    }

    public void setStrMedium(String strMedium) {
        this.strMedium = strMedium;
    }

    public String getStrThumbnail() {
        return strThumbnail;
    }

    public void setStrThumbnail(String strThumbnail) {
        this.strThumbnail = strThumbnail;
    }
}

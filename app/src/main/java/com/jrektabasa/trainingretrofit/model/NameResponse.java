package com.jrektabasa.trainingretrofit.model;

import com.google.gson.annotations.SerializedName;

public class NameResponse {
    @SerializedName("title")
    private String strTitle;
    @SerializedName("first")
    private String strFirst;
    @SerializedName("last")
    private String strLast;

    public NameResponse(String strTitle, String strFirst, String strLast) {
        this.strTitle = strTitle;
        this.strFirst = strFirst;
        this.strLast = strLast;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrFirst() {
        return strFirst;
    }

    public void setStrFirst(String strFirst) {
        this.strFirst = strFirst;
    }

    public String getStrLast() {
        return strLast;
    }

    public void setStrLast(String strLast) {
        this.strLast = strLast;
    }
}

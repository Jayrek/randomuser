package com.jrektabasa.trainingretrofit.model;

import com.google.gson.annotations.SerializedName;

public class RandomUserResponse {
    @SerializedName("name")
    private NameResponse nameResponse;
    @SerializedName("picture")
    private PictureResponse pictureResponse;

    public RandomUserResponse(NameResponse nameResponse, PictureResponse pictureResponse) {
        this.nameResponse = nameResponse;
        this.pictureResponse = pictureResponse;
    }

    public NameResponse getNameResponse() {
        return nameResponse;
    }

    public void setNameResponse(NameResponse nameResponse) {
        this.nameResponse = nameResponse;
    }

    public PictureResponse getPictureResponse() {
        return pictureResponse;
    }

    public void setPictureResponse(PictureResponse pictureResponse) {
        this.pictureResponse = pictureResponse;
    }
}

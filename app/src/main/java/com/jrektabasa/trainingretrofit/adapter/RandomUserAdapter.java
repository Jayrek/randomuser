package com.jrektabasa.trainingretrofit.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jrektabasa.trainingretrofit.R;
import com.jrektabasa.trainingretrofit.model.RandomUserResponse;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RandomUserAdapter extends RecyclerView.Adapter<RandomUserAdapter.MyHyViewHolder> {
    private ArrayList<RandomUserResponse> randomUserResponseArrayList;
    private Context context;

    public RandomUserAdapter(ArrayList<RandomUserResponse> randomUserResponseArrayList, Context context) {
        this.randomUserResponseArrayList = randomUserResponseArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyHyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_user, parent, false);
        return new RandomUserAdapter.MyHyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHyViewHolder holder, int position) {
        RandomUserResponse randomUserResponse = randomUserResponseArrayList.get(position);
        String strFirst = randomUserResponse.getNameResponse().getStrFirst();
        String strLast = randomUserResponse.getNameResponse().getStrLast();
        String strImage = randomUserResponse.getPictureResponse().getStrLarge();

        Glide.with(context).load(strImage).into(holder.ivRandomUserPhoto);
        holder.tvRandomUserName.setText(strFirst + " " + strLast);
    }

    @Override
    public int getItemCount() {
        return randomUserResponseArrayList.size();
    }

    public class MyHyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivRandomUserPhoto;
        private TextView tvRandomUserName;
        public MyHyViewHolder(View itemView) {
            super(itemView);
            ivRandomUserPhoto=(CircleImageView) itemView.findViewById(R.id.ivRandomUserPhoto);
            tvRandomUserName=(TextView) itemView.findViewById(R.id.tvRandomUserName);
        }
    }
}
